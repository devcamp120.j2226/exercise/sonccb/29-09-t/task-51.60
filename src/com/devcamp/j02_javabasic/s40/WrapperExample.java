package com.devcamp.j02_javabasic.s40;

public class WrapperExample {

 public static void autoBoxing() {
   byte bte = 14;
   short sh = 25;
   int it = 36;
   long lng = 47;
   float fat = 58.0f;
   double dbl = 69.0D;
   char ch = 'e';
   boolean bool = true;
   /***
    * //Autoboxing: Converting primitive into objects
    * Autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object
    * của wrapper class tương ứng. 
    */
   Byte byteobj = bte;
   Short shortobj = sh;
   Integer intobj = it;
   Long longobj = lng;
   Float floatobj = fat;
   Double doubleobj = dbl;
   Character charobj = ch;
   Boolean boolobj = bool;

   System.out.println("---Printing object walues (In giá trị của object)---");
   System.out.println("Byte Oject: " + byteobj);
   System.out.println("Short Oject: " + shortobj);
   System.out.println("Integer Oject: " + intobj);
   System.out.println("Long Oject: " + longobj);
   System.out.println("Float Oject: " + floatobj);
   System.out.println("Double Oject: " + doubleobj);
   System.out.println("Character Oject: " + charobj);
   System.out.println("Boolean Oject: " + boolobj);
  }
  public static void unBoxing() {
   byte bte = 14;
   short sh = 25;
   int it = 36;
   long lng = 47;
   float fat = 58.0f;
   double dbl = 69.0D;
   char ch = 'e';
   boolean bool = true;
   //Autoboxing: Converting primitive into objects
   Byte byteobj = bte;
   Short shortobj = sh;
   Integer intobj = it;
   Long longobj = lng;
   Float floatobj = fat;
   Double doubleobj = dbl;
   Character charobj = ch;
   Boolean boolobj = bool;
   /***
    * Unboxing: Converting objects to primitive
    * Unboxing là cơ chế tự động chuyển đổi các object
    * của wrapper class sang kiểu dữ kiệu nguyên thủy tương ứng.
    */
   byte bytevalue = byteobj;
   short shortvalue = shortobj;
   int intvalue = intobj;
   long longvalue = longobj;
   float floatvalue = floatobj;
   double doublevalue = doubleobj;
   char charvalue = charobj;
   boolean boolvalue = boolobj;

   System.out.println("---Printing primitive walues (In giá trị của các Primitive Data types (Kiểu dữ kiệu nguyên thủy))---");
   System.out.println("Byte value: " + bytevalue);
   System.out.println("Short value: " + shortvalue);
   System.out.println("Integer value: " + intvalue);
   System.out.println("Long value: " + longvalue);
   System.out.println("Float value: " + floatvalue);
   System.out.println("Double value: " + doublevalue);
   System.out.println("Character value: " + charvalue);
   System.out.println("Boolean value: " + boolvalue);
 }

 public static void main(String[] args) {
  WrapperExample.autoBoxing();
  WrapperExample.unBoxing();
 }
}
