package com.devcamp.j02_javabasic.s50;

public class PrimitiveToWrapper {
 byte bte = 14;
 short sh = 25;
 int it = 36;
 long lng = 47;
 float fat = 58.0f;
 double dbl = 69.0D;
 char ch = 'e';
 boolean bool = true;
 //Subtask 1:chuyển đổi từ Primitive Data Types sang Wrapper tương ứng.
 Byte byteobj = bte;
 Short shortobj = sh;
 Integer intobj = it;
 Long longobj = lng;
 Float floatobj = fat;
 Double doubleobj = dbl;
 Character charobj = ch;
 Boolean boolobj = bool;
 //Subtask 2 :shuyển đổi từ Wrapper sang PrimitiveData Types tương ứng
 byte bytevalue = byteobj;
 short shortvalue = shortobj;
 int intvalue = intobj;
 long longvalue = longobj;
 float floatvalue = floatobj;
 double doublevalue = doubleobj;
 char charvalue = charobj;
 boolean boolvalue = boolobj;
 public static void main(String[] args) {
  PrimitiveToWrapper obj = new PrimitiveToWrapper();
     System.out.println("Primitive Data Types sang Wrapper");
     System.out.println(obj.byteobj);
     System.out.println(obj.shortobj);
     System.out.println(obj.intobj);
     System.out.println(obj.longobj);
     System.out.println(obj.floatobj);
     System.out.println(obj.doubleobj);
     System.out.println(obj.charobj);
     System.out.println(obj.boolobj);

     System.out.println("Wrapper sang PrimitiveData Types");
     System.out.println(obj.bytevalue);
     System.out.println(obj.shortvalue);
     System.out.println(obj.intvalue);
     System.out.println(obj.longvalue);
     System.out.println(obj.floatvalue);
     System.out.println(obj.doublevalue);
     System.out.println(obj.charvalue);
     System.out.println(obj.boolvalue);
 }
}
