package com.devcamp.j02_javabasic.s30;

public class Customer {
 byte myByte; //byte
 short myShort; // short
 int myInt; // integer whole number
 long myLong; // long
 float myFloat;// floating point number
 double myDouble; //double
 boolean myBoolean; //boolean
 char myLetter; // character

 public Customer(){
 myByte = 127; //byte
 myShort = 123; // short
 myInt = 2023; // integer whole number
 myLong = 12313124; // long
 myFloat = 8.88f;// floating point number
 myDouble = 234.5; //double
 myBoolean = true; //boolean
 myLetter = 's' ; // character
 }

 public Customer( byte byteNum , short shortNum , long longNum, double doubleNum, int intNum, float floatNum , char letter, boolean bool) {
  myByte = byteNum;
  myShort = shortNum;
  myInt = intNum;
  myLong = longNum;
  myFloat = floatNum;
  myDouble = doubleNum;
  myBoolean = bool;
  myLetter = letter;
 }
 public static void main(String[] args) {
  Customer customer = new Customer();
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);
  System.out.println("Run 1:");
  byte myByte1 = 10;
  short myShort1 = 2001;
  customer = new Customer( myByte1 , myShort1, 7001, 671.9 ,231 ,71.99f,'A',false );
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);
  System.out.println("Run 2:");
  byte myByte2 = 20;
  short myShort2 = 2002;
  customer = new Customer( myByte2 , myShort2, 7002, 672.9 ,232 ,72.99f,'B',true );
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);
  System.out.println("Run 3:");
  byte myByte3 = 30;
  short myShort3 = 2003;
  customer = new Customer( myByte3 , myShort3, 7003, 673.9 ,233 ,73.99f,'C',false );
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);
  System.out.println("Run 4:");
  byte myByte4 = 40;
  short myShort4 = 2004;
  customer = new Customer( myByte4 , myShort4, 7004, 674.9 ,234 ,74.99f,'D',true );
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);
  System.out.println("Run 5:");
  byte myByte5 = 50;
  short myShort5 = 2005;
  customer = new Customer( myByte5 , myByte5, 7005, 675.9 ,235 ,75.99f,'E',false );
  System.out.println(customer.myByte);
  System.out.println(customer.myShort);
  System.out.println(customer.myInt);
  System.out.println(customer.myLong);
  System.out.println(customer.myFloat);
  System.out.println(customer.myDouble);
  System.out.println(customer.myBoolean);
  System.out.println(customer.myLetter);

 }
}
